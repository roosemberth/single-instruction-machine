# Single instruction machine simulator

Runs the equivalent to the following python code in a loop (as a single instruction):

> ```
> q = m[m[ip+3]] = m[m[ip+2]] - m[m[ip+1]]
> if q < 0:
>   ip = m[ip]
> else:
>   ip += 4
> ```

#### Input

A list of (signed) integers from stdin (`a`).

#### Output

A List of the traversed states, including the comparison.

#### Example execution

```
$ seq 1 10 | ./sim
ip: 0 mem: [1,2,3,4,5,6,7,8,9,10] -> res: 1
ip: 4 mem: [1,2,3,4,1,6,7,8,9,10] -> res: 1
ip: 8 mem: [1,2,3,4,1,6,7,8,1,10] -> segv 10
```

## Building

Building requires GHC.

```
$ ghc sim.hs
```

## Specificalities

- Memory index starts at zero.
- The only way to stop the machine at the moment is with a segfault.

## License

[GPL 3](https://www.gnu.org/licenses/gpl-3.0.en.html).
