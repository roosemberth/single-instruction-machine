-- (C) Roosembert Palacios, 2020
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.

-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>.

{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE ViewPatterns #-}

import Data.Char

type State = (Int, [Int])

validIdx :: Int -> Int -> Bool
validIdx _   a | a < 0   = False
validIdx top a | top > a = True
validIdx _   _           = False

-- | Calculate an alternate result if the specified function fails.
recoverEmpty :: (a -> b) -> (a -> Maybe c) -> a -> Either b c
recoverEmpty _ try (try -> Just v) = Right v
recoverEmpty recover _ arg         = Left (recover arg)

infix 9 !!?
(!!?) :: [a] -> Int -> Maybe a
(!!?) xs i | validIdx (length xs) i = Just (xs !! i)
(!!?) _ _                           = Nothing

-- | Data type representing the state of a machine at a punctunal moment.
data Machine = Run State | End State | SegFault Int deriving Show

-- | Simulate the execution of a single instruction.
safeStep :: State -> Machine
safeStep = either SegFault Run . transition

-- | Perform a state transition. Left upon illegal.
transition :: State -> Either Int State
transition (eval -> Left segv)                = Left segv
transition p@(eval -> Right (r, idx)) | r < 0 = update idx p $ Left r
transition p@(eval -> Right (r, idx))         = update idx p $ Right r

-- | Intermediary evaluation during a transition. Left upon illegal access.
-- Returns a pair with the assigned result and the new instruction pointer.
eval :: State -> Either Int (Int, Int)
eval (ip, m) = do
  i1  <- recoverEmpty id (m !!?) (ip + 2)
  i2  <- recoverEmpty id (m !!?) (ip + 1)
  i1' <- recoverEmpty id (m !!?) i1
  i2' <- recoverEmpty id (m !!?) i2
  i3  <- recoverEmpty id (m !!?) (ip + 3)
  pure (i1' - i2', i3)

-- | Memory update upon a state transition.
update :: Int -> State -> Either Int Int -> Either Int State
update idx (ip, a) (Left r)  = do
  newIdx <- recoverEmpty id (a !!?) ip
  newMem <- updateList idx a r
  pure (newIdx, newMem)
update idx (ip, a) (Right r) = (,) (ip + 4) <$> updateList idx a r

-- | Helper function to update a list at a given index with the given value.
updateList :: Int -> [a] -> a -> Either Int [a]
updateList idx ls v | validIdx (length ls) idx = go idx ls
  where go 0 (_:as) = Right (v:as)
        go i (a:as) = (a:) <$> go (i-1) as
        go i _ = Left i
updateList i _ _ = Left i

-- | Given an initial state, runs a program upon completion or segfault.
run :: State -> [Machine]
run (safeStep -> SegFault addr) = [SegFault addr]
run (safeStep -> Run s) = (Run s) : run s
run (safeStep -> End s) = [End s]

splitOnSpace :: String -> [String]
splitOnSpace [] = []
splitOnSpace (break isSpace -> (h, [])) = [h]
splitOnSpace (break isSpace -> (h, str)) =
  h : splitOnSpace (removeLeadingSpaces str)
    where removeLeadingSpaces (l:ls) | isSpace l = removeLeadingSpaces ls
          removeLeadingSpaces ls = ls

-- | Prints a list of punctunal machine states.
printExecution :: [Machine] -> IO ()
printExecution [] = pure ()
printExecution (Run st@(ip, mem):ss) = do
  let showEval (Right (res, _)) =  "res: " ++ show res
      showEval (Left faultAddr)  = "segv " ++ show faultAddr
  putStrLn $ mconcat
    [ "ip: ", show ip, " mem: ", show mem
    , " -> ", showEval (eval st)
    ]
  printExecution ss
printExecution (s:ss) = putStrLn (show s) >> printExecution ss

main :: IO ()
main = do
  -- Parse space-separated numbers as stdin
  mem <- fmap (read @Int) . splitOnSpace <$> getContents
  -- Print initial state
  printExecution [Run (0, mem)]
  printExecution $ run (0, mem)
